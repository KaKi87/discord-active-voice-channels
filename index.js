#!/usr/bin/env node

const { version } = require('./package');

const
    consola = require('consola'),
    chalk = require('chalk'),
    prompts = require('prompts'),
    Eris = require('eris');

(async () => {
    consola.info(`Discord : active voice channels, v${version}`);

    const { token } = await prompts({
        type: 'password',
        name: 'token',
        message: 'Input user token'
    });

    const client = new Eris(token);

    client.on('error', consola.error);

    client.on('ready', () => {
        const res = client.guilds.map(guild => ({
            name: guild.name,
            activeVoiceChannels: guild.channels.filter(channel => channel.voiceMembers && channel.voiceMembers.size).map(channel => ({
                name: channel.name,
                members: channel.voiceMembers.map(({ user }) => `${user.username}#${user.discriminator}`)
            }))
        })).filter(guild => guild.activeVoiceChannels.length);

        consola.info('Active voice channels :'
            + res.map(guild => `\n${chalk.blue(guild.name)}\n${guild.activeVoiceChannels.map(channel => `  - (${chalk.yellow(channel.members.length)}) ${channel.name} : ${channel.members.join(', ')}`).join('\n')}`).join('\n'));

        process.exit();
    });

    client.connect()
          .catch(consola.error);
})();